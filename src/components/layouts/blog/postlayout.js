import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby';
import { Helmet } from 'react-helmet';
import SiteNavigation from '../../navigation';
import {
  container,
  heading,
  navLinks,
  navLinkItem,
  navLinkText,
  siteTitle,
} from '../default/layout.module.css';

const PostLayout = ({ pageTitle, children }) => {
  const data = useStaticQuery(graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`)

let navStyles = {
  item:  navLinkItem,
  links: navLinks,
  text: navLinkText
}

let links = [
  {path: "/", text: "Home"},
  {path: "/about", text: "About"},
  {path: "/blog", text: "Blog"},
]
return (

  <div>
    <Helmet>
      <meta charSet="utf-8" />
      <title></title>
    </Helmet>
    <header className={siteTitle}>{data.site.siteMetadata.title}</header>
    <SiteNavigation navLinks={links} navLinkStyles={navStyles}/>
  </div>
)
}

export default PostLayout;