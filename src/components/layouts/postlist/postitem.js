import * as React from 'react'
import { Link } from 'gatsby'
import {
  postHeader,
  coverImage,
  postTitle,
  postDetails,
  buttonGroup,
  button
} from './postitem.module.css'

const PostItem = ({ post, frontmatter }) => {

  return (
    <article>
      <header className={postHeader}>
        <img className={coverImage} src={frontmatter.hero_image} alt={frontmatter.hero_image_alt} />
        <Link to={`./${post.slug}`} className={postTitle}><h2>{frontmatter.title}</h2></Link>
        <p className={postDetails}>
          <span>Posted: {frontmatter.date}</span>
          <span>Word Count: {post.wordCount.words}</span>
        </p>
      </header>
      <p>{frontmatter.description}</p>
      <div className={buttonGroup}>
        <Link to={`./${post.slug}`} className={button}>Read More</Link>
        <Link to={`./${post.slug}`} className={button}>Share</Link>
      </div>
    </article>
  )
}

export default PostItem