import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby';
import {
  container,
  heading,
  siteTitle,
} from './layout.module.css';
import { Helmet } from 'react-helmet';
import SiteNavigation from '../../components/navigation';
import "../../../styles/main.scss";
import Sidebar from '../../components/sidebar/sidebar';

const Layout = ({ pageTitle, children }) => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  let links = [
    {path: "/", text: "Home"},
    {path: "/about", text: "About"},
    {path: "/blog", text: "Blog"},
  ]

  return (
    <div className="container">
      <Helmet>
        <meta charSet="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>{pageTitle} | {data.site.siteMetadata.title}</title>
      </Helmet>
      <header className={siteTitle}>
        {data.site.siteMetadata.title}
        <SiteNavigation navLinks={links}/>
      </header>
      <main>
        {children}
      </main>
      <Sidebar/>
    </div>
  );
};

export default Layout;
