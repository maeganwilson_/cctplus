import * as React from 'react';
import { Link } from 'gatsby';
import {
  bmc,
  patreon
} from './bmcbutton.module.css';

const SupportButton= ({platform}) => {
  let supportTypes = {
    coffee: {
      class: bmc,
      text: "☕ Buy me a Coffee ☕",
      link: "https://checkout.stripe.com/pay/cs_live_a1nWQ9qpPP7UnvsqFVBRTE8F7jkrLtupVfzOyLaclK3hYAtg9FIdD2HDUG#fidkdWxOYHwnPyd1blppbHNgWjVgf39QQGFGU2pPakhHTVFdQEs0R1F8VTU1Ym1ATmByUG0nKSd1aWxrbkB9dWp2YGFMYSc%2FJzQxbjFsdDJiXDZOQWFOUDBubid4JSUl"
    },
    patreon: {
      class: patreon,
      text: "💸 Subsribe to my Patreon 💸",
      link: "https://geni.us/cctplus-patreon"
    }
  }
  console.log(supportTypes[platform.toLowerCase()])
  return (
    <a href={supportTypes[platform.toLowerCase()].link}>
      <button className={supportTypes[platform.toLowerCase()].class}>
        {supportTypes[platform.toLowerCase()].text}
      </button>
    </a>
  )
}

export default SupportButton