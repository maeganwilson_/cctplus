import * as React from 'react';
import { Link } from 'gatsby';
import {
  navLinkList,
  navLinkItem,
  navLinkText,
  active
} from './navigation.module.css';

const SiteNavigation = ({ navLinks }) => {
  return (
    <nav>
      <ul className={navLinkList}>
        {navLinks.map((nav, index) =>
          <li key={index}>
          <Link to={nav.path} className={navLinkItem} activeClassName={active}>
            <span className={navLinkText}>{nav.text}</span>
          </Link>
        </li>)}
      </ul>
    </nav>
  )
}

export default SiteNavigation;