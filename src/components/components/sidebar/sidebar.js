import * as React from 'react';
import SupportButton from '../BMCButton/bmcbutton';

const Sidebar = ({ links }) => {
  return (
    <aside className='sidebar'>
      <h2>Support</h2>
      <p>Help keep this going!</p>
      <SupportButton platform="Coffee"/>
      <SupportButton platform="Patreon"/>
      <h2></h2>
    </aside>
  )
}

export default Sidebar