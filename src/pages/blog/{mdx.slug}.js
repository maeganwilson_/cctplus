import * as React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/layouts/default/layout'
import { MDXRenderer } from 'gatsby-plugin-mdx'

const BlogPost = ({ data }) => {

  return (
    <Layout pageTitle={data.mdx.frontmatter.title}>
      <p>{data.mdx.frontmatter.date}</p>
      <MDXRenderer>{data.mdx.body}</MDXRenderer>
    </Layout>
  )
}

export const query = graphql`
  query ($slug: String){
      mdx(slug: {eq: $slug}) {
        frontmatter {
          hero_image
          date(formatString: "MMMM D, YYYY")
          description
          hero_image_alt
          title
        }
        wordCount {
          words
        }
        body
      }
    }
`

export default BlogPost