import * as React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/layouts/default/layout'
import PostItem from '../../components/layouts/postlist/postitem'

const BlogPage = ({ data }) => {
  return (
    <Layout pageTitle="My Blog Posts">
      <ul>
        {
          data.allMdx.nodes.map(node => (
            <PostItem key={node.id} post={node} frontmatter={node.frontmatter} />
          ))
        }
      </ul>
    </Layout>
  )
}

export default BlogPage

export const query = graphql`
{
  allMdx(sort: {fields: frontmatter___date, order: DESC}){
    nodes {
      slug
      id
      frontmatter {
        hero_image
        hero_image_alt
        date(formatString: "MMMM DD, YYYY", locale: "en")
        description
        title
      }
      wordCount {
        words
      }
    }
  }
}
`
