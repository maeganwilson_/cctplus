import * as React from 'react'
import "../styles/main.scss"
import Layout from '../components/layouts/default/layout'

const IndexPage = () => {
  return (
    <Layout pageTitle="Home Page">
      <p>Your one stop shop for Swift tutorials, software engineering workflows, and posts about gear.</p>
    </Layout>
  )
}

export default IndexPage