import * as React from 'react'
import Layout from '../components/layouts/default/layout'

const AboutPage = () => {
    return (
        <Layout pageTitle="About Me">
            <p>This is all about me</p>
        </Layout>
    )
}

export default AboutPage